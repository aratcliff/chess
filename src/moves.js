const pieceExists = (x, y, gameState, all=false) => gameState.filter(piece => piece.alive || all).find(piece => piece.x === x && piece.y === y);
const sameColourPiece = (x, y, gameState, colour) => pieceExists(x, y, gameState) && pieceExists(x, y, gameState).colour === colour;
const upDown = colour => colour === 'white' ? -1 : 1;

const findPawnMoves = (piece, gameState, kingCheck=false) => {
  const { x, y } = piece;
  const yMult = upDown(piece.colour);
  let moves = [];

  let newY = y + yMult;
  if (!pieceExists(x, newY, gameState) && !kingCheck) {
    moves.push([x, newY]);

    // Initial move can go 2 forwards
    if (!piece.hasMoved && !pieceExists(x, newY + yMult, gameState)) {
      moves.push([x, newY + yMult])
    }
  }

  if (pieceExists(x + 1, newY, gameState) && !sameColourPiece(x + 1, newY, gameState, piece.colour) || kingCheck) {
    moves.push([x + 1, newY]);
  }

  if (pieceExists(x - 1, newY, gameState) && !sameColourPiece(x - 1, newY, gameState, piece.colour) || kingCheck) {
    moves.push([x - 1, newY]);
  }

  return moves;
};

const findRookMoves = (piece, gameState, kingCheck=false) => {
  const { x, y } = piece;
  let moves = [];

  const checkMove = (x, y) => {
    if (sameColourPiece(x, y, gameState, piece.colour)) return true;

    // If not same colour piece, add move
    moves.push([x, y])

    // If piece exists, terminate loop
    if (pieceExists(x, y, gameState) && !kingCheck) return true;
    return false
  }

  for (let i = x + 1; i < 8; i++) {
    if (checkMove(i, y)) break;
  }

  for (let i = x - 1; i >= 0; i--) {
    if (checkMove(i, y)) break;
  }

  for (let i = y + 1; i < 8; i++) {
    if (checkMove(x, i)) break;
  }

  for (let i = y - 1; i >= 0; i--) {
    if (checkMove(x, i)) break;
  }

  return moves;
};

const findKnightMoves = (piece, gameState) => {
  const { x, y } = piece;

  return [
    [ x - 1, y + 2 ], [ x + 1, y + 2 ],
    [ x - 1, y - 2 ], [ x + 1, y - 2 ],
    [ x - 2, y + 1 ], [ x + 2, y + 1 ],
    [ x - 2, y - 1 ], [ x + 2, y - 1 ],
  ].filter(move => {
    if (move[0] < 0 || move[0] > 7 || move[1] < 0 || move[1] > 7) return false;

    return !sameColourPiece(move[0], move[1], gameState, piece.colour);
  })
}

const findBishopMoves = (piece, gameState, kingCheck=false) => {
  const { x, y } = piece;
  let moves = [];

  const checkMove = (x, y) => {
    if (sameColourPiece(x, y, gameState, piece.colour)) return true;

    // If not same colour piece, add move
    moves.push([x, y])

    // If piece exists, terminate loop
    if (pieceExists(x, y, gameState) && !kingCheck) return true;
    return false
  }

  for (let i = x + 1; i < 8; i++) {
    if (checkMove(i, y + i - x)) break;
  }

  for (let i = x + 1; i < 8; i++) {
    if (checkMove(i, y - i + x)) break;
  }

  for (let i = x - 1; i >= 0; i--) {
    if (checkMove(i, y - i + x)) break;
  }

  for (let i = x - 1; i >= 0; i--) {
    if (checkMove(i, y + i - x)) break;
  }

  return moves;
}

const kingMoves = (x, y) => ([
  [ x - 1, y + 1 ], [ x, y + 1 ], [ x + 1, y + 1 ],
  [ x - 1, y ], [ x + 1, y ],
  [ x - 1, y - 1 ], [ x, y - 1 ], [ x + 1, y - 1 ],
]);

const mapMoves = (moves, move) => moves.map(possibleMove => possibleMove.toString()).indexOf(move.toString()) !== -1;

const findKingMoves = (piece, gameState) => {
  let moves = [];

  const oppKing = gameState.find(opp => opp.type === 'king' && opp.colour !== piece.colour)

  const standardMoves = kingMoves(piece.x, piece.y).filter(move => {
    if (move[0] < 0 || move[0] > 7 || move[1] < 0 || move[1] > 7) return false;

    // Check if alive pieces can take king in possible spot
    // Skipping king because it causes an infinite loop
    const moveTaken = gameState.filter(opp => opp.colour !== piece.colour && opp.alive && opp.type !== 'king')
      .map(opp => mapMoves(findMoves(opp, gameState, '', true), move))
      .reduce((acc, taken) => taken || acc, false)

    if (moveTaken) return false;

    // Check king separately
    if (mapMoves(kingMoves(oppKing.x, oppKing.y), move)) return false;

    return !sameColourPiece(move[0], move[1], gameState, piece.colour);
  });

  moves = standardMoves;
  return moves;
};

export const kingChecked = (colour, gameState) => {
  const king = gameState.find(piece => piece.type === 'king' && piece.colour === colour);
  const oppKing = gameState.find(piece => piece.type === 'king' && piece.colour !== colour);

  const kingCanBeTaken = gameState.filter(opp => opp.colour !== colour && opp.alive && opp.type !== 'king')
    .map(opp => mapMoves(findMoves(opp, gameState), [king.x, king.y]))
    .reduce((acc, canTake) => canTake || acc, false);

    return kingCanBeTaken || mapMoves(kingMoves(oppKing.x, oppKing.y), king.x, king.y);
};

export const findMoves = (piece, gameState, overrideType='', kingCheck=false) => {
  const switcher = overrideType ? overrideType : piece.type;
  switch (switcher) {
    case 'pawn':
      return piece.currentPiece === 'pawn' ? findPawnMoves(piece, gameState, kingCheck) : findMoves(piece, gameState, piece.currentPiece, kingCheck);

    case 'rook':
      return findRookMoves(piece, gameState, kingCheck);

    case 'knight':
      return findKnightMoves(piece, gameState);

    case 'bishop':
      return findBishopMoves(piece, gameState, kingCheck);

    case 'queen':
      return [ ...findRookMoves(piece, gameState, kingCheck), ...findBishopMoves(piece, gameState, kingCheck) ];

    case 'king':
      return findKingMoves(piece, gameState);

    default:
      return null;
  }
};