const pawn = (x, y) => ({
  type: 'pawn',
  x, y,
  alive: true,
  hasMoved: false,
  currentPiece: 'pawn',
});

const rook = (x, y) => ({
  type: 'rook',
  x, y,
  alive: true,
  hasMoved: false,
});

const knight = (x, y) => ({
  type: 'knight',
  x, y,
  alive: true,
});

const bishop = (x, y) => ({
  type: 'bishop',
  x, y,
  alive: true,
});

const king = (x, y) => ({
  type: 'king',
  x, y,
  alive: true,
  hasMoved: false,
  checked: false,
});

const queen = (x, y) => ({
  type: 'queen',
  x, y,
  alive: true,
});

export const generateInitialState = (tr, br) => {
  const topRow = Array.from(Array(8).keys()).map(x => pawn(x, tr));
  const bottomRow = [ rook(0, br), knight(1, br), bishop(2, br), queen(3, br), king(4, br), bishop(5, br), knight(6, br), rook(7, br) ];

  return [ ...topRow, ...bottomRow ];
};

export const generateInitialBoard = () => {
  const white = generateInitialState(6, 7).map(piece => ({ ...piece, colour: 'white' }));
  const black = generateInitialState(1, 0).map(piece => ({ ...piece, colour: 'black' }));
  return [ ...white, ...black ];
};
